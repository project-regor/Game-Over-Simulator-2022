using UnityEngine;
using UnityEngine.UIElements;

public class TrackScore : MonoBehaviour
{
    private UIDocument _document;
    private VisualElement _root;
    private Label _p1Score;
    private Label _p2Score;
    
    // Start is called before the first frame update
    void Start()
    {
        _document = GetComponent<UIDocument>();
        _root = _document.rootVisualElement;
        _p1Score = _root.Q<Label>("Player1Score");
        _p2Score = _root.Q<Label>("Player2Score");

        PushmanCore[] players = FindObjectsOfType<PushmanCore>();
        players[0].Death.AddListener(IncrementP1Score);
        players[1].Death.AddListener(IncrementP2Score);
    }

    private void IncrementP1Score()
    {
        _p1Score.text = (int.Parse(_p1Score.text) + 1).ToString();
    }

    private void IncrementP2Score()
    {
        _p2Score.text = (int.Parse(_p2Score.text) + 1).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
