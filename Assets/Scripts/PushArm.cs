using UnityEngine;

public class PushArm : MonoBehaviour
{
    private PushmanCore _core;

    private void Start()
    {
        _core = GetComponentInParent<PushmanCore>();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (_core.ARMState is not (PushmanCore.ArmState.FullyExtended or PushmanCore.ArmState.Extending) || 
            !other.transform.CompareTag("Player") && !other.transform.CompareTag("PushmanArm")) return;
        
        Rigidbody rb = other.transform.GetComponentInParent<Rigidbody>();
        rb.AddForce(transform.forward * _core.PushForce, ForceMode.Impulse);
    }
}