using System;
using System.Collections.ObjectModel;
using System.Linq;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class AgentController : Agent
{
    private PushmanCore _core;
    public PushmanCore opponent;
    public event Action OnReset;

    public override void Initialize()
    {
        _core = GetComponent<PushmanCore>();
        opponent = FindObjectsOfType<PushmanCore>().First(p => p != _core);
        opponent.Death.AddListener(PlayerDied);
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        ActionSegment<int> acts = actions.DiscreteActions;
        Vector3 move = new();

        if (acts[0] == 1)
            _core.Push();
        switch (acts[1])
        {
            case 1:
                move.z = 1; 
                break;
            case 2:
                move.z = -1;
                break;
        }
        switch (acts[2])
        {
            case 1:
                move.x = -1; 
                break;
            case 2:
                move.x = 1;
                break;
        }

        _core.Move = move;
    }

    private void FixedUpdate()
    {
        if (_core.ARMState == PushmanCore.ArmState.FullyRetracted)
            RequestDecision();
    }

    private void Reset()
    {
        _core.Respawn();
    }

    public override void OnEpisodeBegin()
    {
        print("EpisodeBegin");
        Reset();
    }

    /*public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.position.x);
        sensor.AddObservation(transform.position.z);
        sensor.AddObservation(transform.rotation.y);
        sensor.AddObservation(opponent.transform.position.z);
        sensor.AddObservation(opponent.transform.position.z);
    }*/

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<int> discreteActionsOut = actionsOut.DiscreteActions;
        discreteActionsOut[0] = 0;
        discreteActionsOut[1] = 0;
        discreteActionsOut[1] = 0;
        discreteActionsOut[2] = 0;
        discreteActionsOut[2] = 0;

        if (Input.GetKey(KeyCode.Space))
        {
            discreteActionsOut[0] = 1;
        }

        if (Input.GetKey(KeyCode.W))
        {
            discreteActionsOut[1] = 1;
        }

        if (Input.GetKey(KeyCode.S))
        {
            discreteActionsOut[1] = 2;
        }

        if (Input.GetKey(KeyCode.A))
        {
            discreteActionsOut[2] = 1;
        }

        if (Input.GetKey(KeyCode.D))
        {
            discreteActionsOut[2] = 2;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Kill")) return;
        
        AddReward(-1.0f);
        print(GetCumulativeReward());
        EndEpisode();
    }

    private void PlayerDied()
    {
        AddReward(20.0f);
        print(GetCumulativeReward());
    }
}