﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class PushmanCore : MonoBehaviour
{
    public UnityEvent Death;

    public enum ArmState
    {
        FullyRetracted,
        Extending,
        FullyExtended,
        Retracting
    }
    
    public float MoveSpeed;
    public float TurnSpeed;
    public float PushSpeed;
    public float PushForce;
    public float PushDelay;
    
    private Vector3 _initPos;
    private Quaternion _initRot;
    private Rigidbody _rb;

    [SerializeField] private Transform _arm;
    [SerializeField] private float _armMaxPos;
    private float _armMinPos;
    public ArmState ARMState { get; private set; } = ArmState.FullyRetracted;
    
    public Vector3 Move { get; set; }
    public Vector3 Look { get; set; }

    public void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _initPos = transform.position;
        _initRot = transform.rotation;
        _armMinPos = _arm.localPosition.z;
    }

    public void Respawn()
    {
        transform.position = _initPos;
        transform.rotation = _initRot;
        _rb.velocity = Vector3.zero;
        _rb.angularVelocity = Vector3.zero;
        
        Death.Invoke();
    }

    public void Push()
    {
        if(ARMState != ArmState.FullyRetracted) return;

        StartCoroutine(ExtendArm());
    }

    private IEnumerator ExtendArm()
    {
        ARMState = ArmState.Extending;
        
        yield return new WaitUntil(() => ARMState == ArmState.FullyExtended);
        yield return new WaitForSeconds(PushDelay);

        ARMState = ArmState.Retracting;
    }

    private void FixedUpdate()
    {
        _rb.AddForce(Move * (MoveSpeed * Time.deltaTime));
        transform.LookAt(transform.position + Look);

        Vector3 localPos = _arm.localPosition;
        switch (ARMState)
        {
            case ArmState.Extending:
                localPos = new Vector3(localPos.x, localPos.y, localPos.z + PushSpeed * Time.deltaTime);

                if (localPos.z >= _armMaxPos)
                {
                    ARMState = ArmState.FullyExtended;
                    localPos.z = _armMaxPos;
                }
                
                _arm.localPosition = localPos;
                break;
            
            case ArmState.Retracting:
                localPos = new Vector3(localPos.x, localPos.y, localPos.z - PushSpeed * Time.deltaTime);

                if (localPos.z <= _armMinPos)
                {
                    ARMState = ArmState.FullyRetracted;
                    localPos.z = _armMinPos;
                }
                
                _arm.localPosition = localPos;
                break;
        }
    }

   /* private void OnCollisionEnter(Collision other)
    {
        print(other.transform.tag);
        if (!other.transform.CompareTag("PushmanArm")) return;

        PushmanCore otherCore = other.transform.GetComponentInParent<PushmanCore>();
        if(otherCore.ARMState is not (ArmState.FullyExtended or ArmState.Extending)) return;

        print("YEEY");
        _rb.AddForce(other.transform.forward * otherCore.PushForce, ForceMode.Impulse);
    }*/
}