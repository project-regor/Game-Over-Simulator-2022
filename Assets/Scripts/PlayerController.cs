using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private PushmanCore _core;

    void OnMove(InputValue input)
    {
        Vector2 moveInput = input.Get<Vector2>();
        _core.Move = new Vector3(moveInput.x, 0, moveInput.y);
    }

    void OnLook(InputValue input)
    { 
        Vector2 look = input.Get<Vector2>();
        _core.Look = new Vector3(look.x, 0, look.y);
    }

    void OnPush()
    {
        _core.Push();    
    }
    
    void Start()
    {
        _core = GetComponent<PushmanCore>();
        Cursor.lockState = CursorLockMode.Locked;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Kill"))
            _core.Respawn();
    }
}
